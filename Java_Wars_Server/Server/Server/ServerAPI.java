package Server;

import java.util.ArrayList;

import Game.Team;
import Map.Map;
import other.Coordinates;
import other.Grid2D;

public interface ServerAPI 
{
	/**
	 * login to server with a nickname
	 * @param nickname
	 */
	public void login(String nickname);
	/**
	 * @return list of available games, <id, name> 
	 */
	public ArrayList<String> getListOfGames();
	/**
	 * connection to a game on server
	 * @param gameName
	 * @return id ingame
	 */
	public int connectToGame(String gameName);
	/**
	 * Create a game
	 * @param map chosen for the game
	 * @param liste of teams
	 */
	public void createGame(Map map, ArrayList<Team> equipes);
	/**
	 * grives 
	 * @return the name of the map file
	 */
	public String getMapName();
	/**
	 * list of possibles actions to perform on case
	 * @param from from case
	 * @param to to case 
	 * @return list of action names
	 */
	public ArrayList<String> getPossiblesActions(int id, Coordinates from, Coordinates to);
	/**
	 * @param coord
	 * @return
	 */
	public ArrayList<Coordinates>  getPossibleMoves(Coordinates coord);
	/**
	 * finishes the turn
	 */
	public void endTurn(int id);
	/**
	 * surrend the game
	 */
	public void surrend(String id);
}

// API CLIENT
/*
	selectAction(String s);
	endTurn();
*/