package Server;

import java.util.ArrayList;

import Constants.UnitTypes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import Game.Game;
import Game.Player;
import Game.Team;
import Map.Map;
import other.Coordinates;
import other.Grid2D;


/**
 * A server for a network multi-player tic tac toe game.  Modified and
 * extended from the class presented in Deitel and Deitel "Java How to
 * Program" book.  I made a bunch of enhancements and rewrote large sections
 * of the code.  The main change is instead of passing *data* between the
 * client and server, I made a TTTP (tic tac toe protocol) which is totally
 * plain text, so you can test the game with Telnet (always a good idea.)
 * The strings that are sent in TTTP are:
 *
 *  Client -> Server           Server -> Client
 *  ----------------           ----------------
 *  MOVE <n>  (0 <= n <= 8)    WELCOME <char>  (char in {X, O})
 *  QUIT                       VALID_MOVE
 *                             OTHER_PLAYER_MOVED <n>
 *                             VICTORY
 *                             DEFEAT
 *                             TIE
 *                             MESSAGE <text>
 *
 * A second change is that it allows an unlimited number of pairs of
 * players to play.
 */

public class Server implements ServerAPI {
	/**
	 * liste des parties du serveur
	 */
	private ArrayList<Game> myGames;

	@Override
	public void login(String nickname) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<String> getListOfGames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int connectToGame(String gameName) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void createGame(Map map, ArrayList<Team> equipes) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Grid2D<String> getMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getPossiblesActions(int id, Coordinates from, Coordinates to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Coordinates> getPossibleMoves(Coordinates coord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void endTurn(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surrend(String id) {
		// TODO Auto-generated method stub
		
	}

	
	


}
