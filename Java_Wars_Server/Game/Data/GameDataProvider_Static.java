package Data;

import Constants.LoggingLevel;
import Constants.UnitTypes;
import Logs.LoggerSingleton;
import Map.Building;
import Map.BuildingData;
import Map.EnvironmentData;
import Units.UnitData;
import Constants.CaseTypes;

public class GameDataProvider_Static implements GameDataProvider {

	@Override
	public BuildingData getBuildingData(CaseTypes type) {
		BuildingData bd = new BuildingData();
		switch (type)
		{
			case CAPITAL :
				bd.name = "Q.G.";
				bd.desc = "Capture this building to win the game.";
				bd.income = 2000;
				bd.visionRange = 0;
				bd.protection = 5;
				bd.spriteName = "building_capital_color";
				break;
			case CITY :
				bd.name = "City";
				bd.desc = "Capture this building to grant an income.";
				bd.income = 1000;
				bd.visionRange = 0;
				bd.protection = 3;
				bd.spriteName = "building_city_color";
				break;
			case FACTORY :
				bd.name = "Factory";
				bd.desc = "Produces gournd units";
				bd.income = 0;
				bd.visionRange = 0;
				bd.protection = 3;
				bd.spriteName = "building_factory_color";
				break;
			default :
					LoggerSingleton.log(LoggingLevel.ERROR, this.getClass().getName(), "Cannot find matching building for : " + type.toString());
		}
		return bd;
	}

	@Override
	public EnvironmentData getEnvironmentData(CaseTypes type) {
		EnvironmentData ed = new EnvironmentData();
		switch (type)
		{
			case FOREST :
				ed.name = "Forest";
				ed.protection = 2;
				ed.spriteName = "environment_forest";
				break;
			case ROAD :
				ed.name = "Road";
				ed.protection = 0;
				ed.spriteName = "environment_road";
				break;
			case GROUND :
				ed.name = "Ground";
				ed.protection = 1;
				ed.spriteName = "environment_ground";
				break;
			case MOUNTAIN :
				ed.name = "Mountain";
				ed.protection = 4;
				ed.spriteName = "environment_mountain";
				break;
			case WATER :
				ed.name = "Water";
				ed.protection = 0;
				ed.spriteName = "environment_water";
				break;
			default :
					LoggerSingleton.log(LoggingLevel.ERROR, this.getClass().getName(), "Cannot fin matching environment for : " + type.toString());
		}
		return ed;
	}

	@Override
	public UnitData getUnitData(UnitTypes unit)
	{
		UnitData ud = new UnitData();
		ud.name = "Infantry";
		ud.spriteName = "infantry_color";
		ud.rangeMin = 0;
		ud.rangeMax = 1;
		ud.moveRange = 3;
		ud.degats.put(UnitTypes.infantry, 15);
		return ud;
	}
}
