package Data;

import java.io.*;

/**
 * Parses a basic text file
 * @author j_fon
 *
 */
public class BasicTxtParser {
	/**
	 * current file
	 */
	private BufferedReader br;
	/**
	 * indicates if file is opened
	 */
	private boolean fileOpened;
	/**
	 * current red line
	 * null if no line
	 */
	private String currentLine;
	/**
	 * Tells if a file is opened or not
	 * @return true for opened, false if not
	 */
	public boolean isFileOpened()
	{
		return fileOpened;
	}
	/**
	 * opens a txt file
	 * @param filePath path of the file to parse
	 * @return true if the file has correctly been opened, false if not
	 */
	public boolean openFile(String filePath)
	{
		InputStream ips;
		// open file
		try 
		{
			ips = new FileInputStream(filePath);
			InputStreamReader ipsr=new InputStreamReader(ips);
			br=new BufferedReader(ipsr);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * closes current file
	 * if not files, opened : do nothing
	 * @return true if file closed or no file, false if a problem occured
	 */
	public boolean closeFile()
	{
		// test if file is instanciated
		if (br != null)
		{
			try 
			{
				// close file
				br.close();
			} 
			catch (IOException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	/**
	 * Gives and consumes the next line of the file
	 * @return next  String line of the file, null if EOF
	 */
	public String nextLine()
	{
		try 
		{
			return br.readLine();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
}
