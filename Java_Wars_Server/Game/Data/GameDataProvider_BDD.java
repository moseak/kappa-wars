package Data;

import Constants.CaseTypes;
import Constants.LoggingLevel;
import Constants.UnitTypes;
import Logs.LoggerSingleton;
import Map.BuildingData;
import Map.EnvironmentData;
import Units.UnitData;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class GameDataProvider_BDD implements GameDataProvider{
	private static Map<CaseTypes , BuildingData> bds;
	private static Map<CaseTypes , EnvironmentData> eds;
	private static Map<UnitTypes , UnitData> uds;
	Connection myConn;
	public GameDataProvider_BDD()
	{
		bds = new HashMap<CaseTypes , BuildingData>();
		eds = new HashMap<CaseTypes , EnvironmentData>();
		uds = new HashMap<UnitTypes , UnitData>();
		
		try {
			myConn = DriverManager.getConnection("jdbc:mysql://51.255.47.72:3306/kappa","damien","root");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	protected void finalize()
	{
		try {
			myConn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public BuildingData getBuildingData(CaseTypes type) {
		try{
			//Connexion
			//Connection myConn = DriverManager.getConnection("jdbc:mysql://51.255.47.72:3306/kappa","damien","root");
			// creation statement
			Statement myStat = myConn.createStatement();
			
			ResultSet myRes;
			
			BuildingData bd = new BuildingData();
			switch (type)
			{
				case CAPITAL :
					if(bds.get(CaseTypes.CAPITAL) == null)
					{
						myRes = myStat.executeQuery("Select * from kappa.building where name = 'Q.G';");
						// Result Set
						while (myRes.next())
						{
							bd.name = myRes.getString("name");
							//System.out.println(myRes.getString("name"));
							bd.desc = myRes.getString("desc");
							bd.income = Integer.parseInt(myRes.getString("income"));
							bd.visionRange = Integer.parseInt(myRes.getString("visionRange"));
							bd.protection = Integer.parseInt(myRes.getString("protection"));
							bd.spriteName = myRes.getString("spriteName");
						}
						bds.put(CaseTypes.CAPITAL, bd);
					}
					else
					{
						bd = bds.get(CaseTypes.CAPITAL);
						
					}
					break;
				case CITY :
					if(bds.get(CaseTypes.CITY) == null)
					{
						myRes = myStat.executeQuery("Select * from kappa.building where name = 'City';");
						// Result Set
						while (myRes.next())
						{
							bd.name = myRes.getString("name");
							bd.desc = myRes.getString("desc");
							bd.income = Integer.parseInt(myRes.getString("income"));
							bd.visionRange = Integer.parseInt(myRes.getString("visionRange"));
							bd.protection = Integer.parseInt(myRes.getString("protection"));
							bd.spriteName = myRes.getString("spriteName");
							//System.out.println(myRes.getString("spriteName"));
						}
						bds.put(CaseTypes.CITY, bd);
					}
					else
					{
						bd = bds.get(CaseTypes.CITY);
					}
					
					break;
				case FACTORY :
					if(bds.get(CaseTypes.FACTORY) == null)
					{
						myRes = myStat.executeQuery("Select * from kappa.building where name = 'Factory';");
						// Result Set
						while (myRes.next())
						{
							bd.name = myRes.getString("name");
							bd.desc = myRes.getString("desc");
							bd.income = Integer.parseInt(myRes.getString("income"));
							bd.visionRange = Integer.parseInt(myRes.getString("visionRange"));
							bd.protection = Integer.parseInt(myRes.getString("protection"));
							bd.spriteName = myRes.getString("spriteName");
						}
						bds.put(CaseTypes.FACTORY, bd);
					}
					else
					{
						bd = bds.get(CaseTypes.FACTORY);
					}
					break;
				default :
						LoggerSingleton.log(LoggingLevel.ERROR, this.getClass().getName(), "Cannot find matching building for : " + type.toString());
			}
			
			//myConn.close();
			return bd.copy();
		}
		catch(Exception exc)
		{
			System.out.println("dans le catch");
			exc.printStackTrace();
		}
		System.out.println("apres catch");
		return null;
	}

	@Override
	public EnvironmentData getEnvironmentData(CaseTypes type) {
		try{
			//Connexion
			//Connection myConn = DriverManager.getConnection("jdbc:mysql://51.255.47.72:3306/kappa","damien","root");
			// creation statement
			Statement myStat = myConn.createStatement();
			
			ResultSet myRes;
			
			EnvironmentData ed = new EnvironmentData();
			switch (type)
			{
			case FOREST :
				if(eds.get(CaseTypes.FOREST) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.environment where name = 'Forest';");
					while (myRes.next())
					{	
						ed.name = myRes.getString("name");
						ed.protection = Integer.parseInt(myRes.getString("protection"));
						ed.spriteName = myRes.getString("spriteName");
					}
					eds.put(CaseTypes.FOREST, ed);
				}
				else
				{
					ed = eds.get(CaseTypes.FOREST);
				}
				break;
			case ROAD :
				if(eds.get(CaseTypes.ROAD) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.environment where name = 'Road';");
					while (myRes.next())
					{
						ed.name = myRes.getString("name");
						ed.protection = Integer.parseInt(myRes.getString("protection"));
						ed.spriteName = myRes.getString("spriteName");
					}
					eds.put(CaseTypes.ROAD, ed);
				}
				else
				{
					ed = eds.get(CaseTypes.ROAD);
				}
				break;
			case GROUND :
				if(eds.get(CaseTypes.GROUND) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.environment where name = 'Ground';");
					while (myRes.next())
					{
						ed.name = myRes.getString("name");
						ed.protection = Integer.parseInt(myRes.getString("protection"));
						ed.spriteName = myRes.getString("spriteName");
					}
					eds.put(CaseTypes.GROUND, ed);
				}
				else
				{
					ed = eds.get(CaseTypes.GROUND);
				}
				break;
			case MOUNTAIN :
				if(eds.get(CaseTypes.MOUNTAIN) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.environment where name = 'Mountain';");
					while (myRes.next())
					{
						ed.name = myRes.getString("name");
						ed.protection = Integer.parseInt(myRes.getString("protection"));
						ed.spriteName = myRes.getString("spriteName");
					}
					eds.put(CaseTypes.MOUNTAIN, ed);
				}
				else
				{
					ed = eds.get(CaseTypes.MOUNTAIN);
				}
				break;
			case WATER :
				if(eds.get(CaseTypes.WATER) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.environment where name = 'Water';");
					while (myRes.next())
					{
						ed.name = myRes.getString("name");
						ed.protection = Integer.parseInt(myRes.getString("protection"));
						ed.spriteName = myRes.getString("spriteName");
					}
					eds.put(CaseTypes.WATER, ed);
				}
				else
				{
					ed = eds.get(CaseTypes.WATER);
				}
				break;
			default :
					LoggerSingleton.log(LoggingLevel.ERROR, this.getClass().getName(), "Cannot fin matching environment for : " + type.toString());
			}
			
			//myConn.close();
			return ed;
		}
		catch(Exception exc)
		{
			System.out.println("dans le catch");
			exc.printStackTrace();
		}
		System.out.println("apres catch");
		return null;
	}

	@Override
	public UnitData getUnitData(UnitTypes unit) {
		// TODO Auto-generated method stub
		UnitData ud = new UnitData();
		try{
			//Connexion
			//Connection myConn = DriverManager.getConnection("jdbc:mysql://51.255.47.72:3306/kappa","damien","root");
			// creation statement
			Statement myStat = myConn.createStatement();
			
			ResultSet myRes;
			
			EnvironmentData ed = new EnvironmentData();
			switch (unit)
			{
			case infantry :
				if(uds.get(UnitTypes.infantry) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.unite where name = 'Infantry';");
					while (myRes.next())
					{	
						ud.name = myRes.getString("name");
						ud.spriteName = myRes.getString("spriteName");
						ud.moveRange = Integer.parseInt(myRes.getString("moveRange"));
						ud.pv= Integer.parseInt(myRes.getString("pv"));
						ud.visionRange= Integer.parseInt(myRes.getString("visionRange"));
						ud.rangeMax= Integer.parseInt(myRes.getString("rangeMax"));
						ud.rangeMin= Integer.parseInt(myRes.getString("rangeMin"));
						ud.price = Integer.parseInt(myRes.getString("price"));
						
					}
					myRes=myStat.executeQuery("Select * from kappa.attack, kappa.unite where unite.idUnite = attack.attacker and unite.name = 'Infantry';");
					while (myRes.next())
					{
						switch(myRes.getString("attacked"))
						{
						case "0" : 
							ud.degats.put(UnitTypes.infantry, Integer.parseInt(myRes.getString("damage")));
							break;
						case "1" :
							ud.degats.put(UnitTypes.mech, Integer.parseInt(myRes.getString("damage")));
							break;
						case "2" : 
							ud.degats.put(UnitTypes.tank, Integer.parseInt(myRes.getString("damage")));
							break;
						case "3" :
							ud.degats.put(UnitTypes.recoon, Integer.parseInt(myRes.getString("damage")));
							break;
						}
					}
					uds.put(UnitTypes.infantry, ud);
				}
				else
				{
					ud = uds.get(UnitTypes.infantry);
				}
				break;
			case mech :
				if(uds.get(UnitTypes.mech) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.unite where name = 'Mech';");
					while (myRes.next())
					{
						ud.name = myRes.getString("name");
						ud.spriteName = myRes.getString("spriteName");
						ud.moveRange = Integer.parseInt(myRes.getString("moveRange"));
						ud.pv= Integer.parseInt(myRes.getString("pv"));
						ud.visionRange= Integer.parseInt(myRes.getString("visionRange"));
						ud.rangeMax= Integer.parseInt(myRes.getString("rangeMax"));
						ud.rangeMin= Integer.parseInt(myRes.getString("rangeMin"));
						ud.price = Integer.parseInt(myRes.getString("price"));
						
					}
					myRes=myStat.executeQuery("Select * from kappa.attack, kappa.unite where unite.idUnite = attack.attacker and unite.name = 'Mech';");
					while (myRes.next())
					{
						switch(myRes.getString("attacked"))
						{
						case "0" : 
							ud.degats.put(UnitTypes.infantry, Integer.parseInt(myRes.getString("damage")));
							break;
						case "1" :
							ud.degats.put(UnitTypes.mech, Integer.parseInt(myRes.getString("damage")));
							break;
						case "2" : 
							ud.degats.put(UnitTypes.tank, Integer.parseInt(myRes.getString("damage")));
							break;
						case "3" :
							ud.degats.put(UnitTypes.recoon, Integer.parseInt(myRes.getString("damage")));
							break;
						}
					}
					uds.put(UnitTypes.mech, ud);
				}
				else
				{
					ud = uds.get(UnitTypes.mech);
				}
				break;
			case tank :
				if(uds.get(UnitTypes.tank) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.unite where name = 'Light Tank';");
					while (myRes.next())
					{
						ud.name = myRes.getString("name");
						ud.spriteName = myRes.getString("spriteName");
						ud.moveRange = Integer.parseInt(myRes.getString("moveRange"));
						ud.pv= Integer.parseInt(myRes.getString("pv"));
						ud.visionRange= Integer.parseInt(myRes.getString("visionRange"));
						ud.rangeMax= Integer.parseInt(myRes.getString("rangeMax"));
						ud.rangeMin= Integer.parseInt(myRes.getString("rangeMin"));
						ud.price = Integer.parseInt(myRes.getString("price"));
						
					}
					myRes=myStat.executeQuery("Select * from kappa.attack, kappa.unite where unite.idUnite = attack.attacker and unite.name = 'Light Tank';");
					while (myRes.next())
					{
						switch(myRes.getString("attacked"))
						{
						case "0" : 
							ud.degats.put(UnitTypes.infantry, Integer.parseInt(myRes.getString("damage")));
							break;
						case "1" :
							ud.degats.put(UnitTypes.mech, Integer.parseInt(myRes.getString("damage")));
							break;
						case "2" : 
							ud.degats.put(UnitTypes.tank, Integer.parseInt(myRes.getString("damage")));
							break;
						case "3" :
							ud.degats.put(UnitTypes.recoon, Integer.parseInt(myRes.getString("damage")));
							break;
						}
					}
					uds.put(UnitTypes.tank, ud);
				}
				else
				{
					ud = uds.get(UnitTypes.tank);
				}
				break;
			case recoon :
				if(uds.get(UnitTypes.recoon) == null)
				{
					myRes = myStat.executeQuery("Select * from kappa.unite where name = 'Recon';");
					while (myRes.next())
					{
						ud.name = myRes.getString("name");
						ud.spriteName = myRes.getString("spriteName");
						ud.moveRange = Integer.parseInt(myRes.getString("moveRange"));
						ud.pv= Integer.parseInt(myRes.getString("pv"));
						ud.visionRange= Integer.parseInt(myRes.getString("visionRange"));
						ud.rangeMax= Integer.parseInt(myRes.getString("rangeMax"));
						ud.rangeMin= Integer.parseInt(myRes.getString("rangeMin"));
						ud.price = Integer.parseInt(myRes.getString("price"));
						
					}
					myRes=myStat.executeQuery("Select * from kappa.attack, kappa.unite where unite.idUnite = attack.attacker and unite.name = 'Recon';");
					while (myRes.next())
					{
						switch(myRes.getString("attacked"))
						{
						case "0" : 
							ud.degats.put(UnitTypes.infantry, Integer.parseInt(myRes.getString("damage")));
							break;
						case "1" :
							ud.degats.put(UnitTypes.mech, Integer.parseInt(myRes.getString("damage")));
							break;
						case "2" : 
							ud.degats.put(UnitTypes.tank, Integer.parseInt(myRes.getString("damage")));
							break;
						case "3" :
							ud.degats.put(UnitTypes.recoon, Integer.parseInt(myRes.getString("damage")));
							break;
						}
					}
					uds.put(UnitTypes.recoon, ud);
				}
				else
				{
					ud = uds.get(UnitTypes.recoon);
				}
				break;
			default :
					LoggerSingleton.log(LoggingLevel.ERROR, this.getClass().getName(), "Cannot fin matching environment for : " + unit.toString());
			}
			
			//myConn.close();
			return ud.copy();
		}
		catch(Exception exc)
		{
			System.out.println("dans le catch");
			exc.printStackTrace();
		}
		System.out.println("apres catch");
		return null;
	}
	
	

}
