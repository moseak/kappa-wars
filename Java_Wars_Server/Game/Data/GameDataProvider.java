package Data;

import Constants.CaseTypes;
import Constants.UnitTypes;
import Map.BuildingData;
import Map.EnvironmentData;
import Units.UnitData;

/**
 * provides game information datas 
 * @author j_fon
 */
public interface GameDataProvider {
	/**
	 * Provides building datas from a source
	 * @param request string to provide to the source of data
	 * @return BuildingData datas of the chosen building
	 */
	public BuildingData getBuildingData(CaseTypes type);

	public EnvironmentData getEnvironmentData(CaseTypes type);
	
	public UnitData getUnitData(UnitTypes unit);
}
