package Data;

import Constants.CaseTypes;
import Constants.LoggingLevel;

public class MapCaseTranslator {
	public static CaseTypes translate(String s)
	{
		switch (s)
		{
			case "b" :
				return CaseTypes.BEACH;
			case "c" :
				return CaseTypes.CAPITAL;
			case "C" :
				return CaseTypes.CITY;
			case "f" :
				return CaseTypes.FACTORY;
			case "F" :
				return CaseTypes.FOREST;
			case "g" :
				return CaseTypes.GROUND;
			case "m" :
				return CaseTypes.MOUNTAIN;
			case "r" :
				return CaseTypes.REEF;
			case "R" :
				return CaseTypes.ROAD;
			case "w" :
				return CaseTypes.WATER;
			default :
				return CaseTypes.UNKNOWN;
		}
	}
}
