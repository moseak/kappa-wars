package Units;

import java.util.HashMap;
import java.util.Map;

import Constants.Domains;
import Constants.UnitTypes;
import Game.ElementData;

/**
 * datas of an unit
 * @author j_fon
 */
public class UnitData extends ElementData{
	/**
	 * range of vision granted by this unit
	 */
	public int visionRange;
	/**
	 * minimal range of attack
	 */
	public int rangeMin;
	/**
	 * maximal range of attack
	 */
	public int rangeMax;
	/**
	 * domains
	 */
	Domains domain;
	/**
	 * damages dealt on an unit
	 */
	public Map<UnitTypes, Integer> degats;
	/**
	 * pv
	 */
	public int pv;
	
	public int price;
	
	public int moveRange;
	
	public UnitData()
	{
		degats = new HashMap<UnitTypes, Integer>();
	}
	
	public UnitData copy()
	{
		UnitData ud = new UnitData();
		ud.degats=this.degats;
		ud.desc = this.desc;
		ud.moveRange=this.moveRange;
		ud.name = this.name;
		ud.price = this.price;
		ud.pv = this.pv;
		ud.rangeMax = this.rangeMax;
		ud.rangeMin = this.rangeMin;
		ud.spriteName = this.spriteName;
		ud.visionRange = this.visionRange;
		
		return ud;
		
	}
	
	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "NON IMPLEMENTE";
	}
}
