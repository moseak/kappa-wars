package Units;

import Constants.Domains;
import Constants.Types;
import Constants.UnitTypes;
import Game.Element;
/**
 * units of a game
 * @author j_fon
 */
public class Unit implements Element{
	/**
	 * data of an unit
	 */
	public UnitData data;
	/**
	 * type of unit
	 */
	public UnitTypes type;
	/**
	 * team of the unit
	 */
	public int team;
	/*
	 * have the unit already played during this move
	 */
	public boolean hasPlayed;
	/*
	 * have the unit already moved during this move
	 */
	public boolean hasMoved;
	/**
	 * constructor
	 * @param ud
	 */
	public Unit(UnitTypes type, UnitData ud, int team)
	{
		this.type = type;
		this.data = ud;
		this.team = team;
	}
	public int getDamageOn(UnitTypes ut)
	{
		return 0;
	}
	/**
	 * retourne le nombre d'elements en vies dans l'unit�
	 * @return
	 */
	public int getNumberOf()
	{
		float nb = (float)data.pv / 10;
		return (int) (Math.round(nb) ); 
	}
}
