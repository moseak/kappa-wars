package Units;

import Constants.CaseTypes;
import Constants.Colors;
import Constants.StringToEnum;
import Constants.UnitTypes;
import Data.GameDataProvider;
import Data.GameDataProvider_BDD;
import Data.GameDataProvider_Static;
import Data.MapCaseTranslator;
import Game.Rules;
import Map.Building;
import Map.BuildingData;
import Map.Case;
import Map.EnvironmentData;
import Map.Evironment;

public class UnitFactory {
	/**
	 * provider of game data
	 */
	private GameDataProvider gdp;
	public UnitFactory()
	{
		gdp = new GameDataProvider_BDD();
	}
	public Unit createUnit(String unitTypeName, int team)
	{
		UnitTypes ut = StringToEnum.getUnitType(unitTypeName);
		Unit u = new Unit(ut , gdp.getUnitData(ut), team);
		// update sprite color
		Colors color = Rules.getTeamColor(team);
		u.data.spriteName = u.data.spriteName.replace("color", color.toString());
		u.hasPlayed = true;
		return u;
	}
}
