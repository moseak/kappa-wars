package Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

import Constants.UnitTypes;
import other.*;

class Player extends Thread {
	int id;
	Player opponent;
	Socket socket;
	BufferedReader input;
	PrintWriter output;

	Game gameJouee;

	/**
	 * Constructs a handler thread for a given socket and mark
	 * initializes the stream fields, displays the first two
	 * welcoming messages.
	 */
	public Player(Socket socket, int id, Game game) {
		this.socket = socket;
		this.id = id;
		this.gameJouee = game;
		try {
			input = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(), true);
			output.println("WELCOME " + id);
			output.println("MESSAGE Waiting for opponent to connect");

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Player died: " + e);
		}
	}

	/**
	 * Accepts notification of who the opponent is.
	 */
	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}

	/**
	 * Handles the otherPlayerMoved message.
	 */
	public void otherPlayerMoved(int location) {
		//        output.println("OPPONENT_MOVED " + location);
		//        output.println(
		//            Game.hasWinner() ? "DEFEAT" : boardFilledUp() ? "TIE" : "");
	}

	/**
	 * The run method of this thread.
	 */
	public void run() {
		try {
			// The thread is only started after everyone connects.
			output.println("MESSAGE All players connected");

			// Tell the first player that it is her turn.
			if (id == 1) {
				output.println("MESSAGE Your move");
			}

			// Repeatedly get commands from the client and process them.
			while (true) {
				String command = input.readLine();
				//System.out.println("salut " + command);
				if (command != null) {
					String[] splited = command.split("\\s+");
					String commande = splited[0];

					String resu;
					int locationX;
					int locationY;
					int locationX2;
					int locationY2;
					Coordinates coordDepart;
					Coordinates coordArrivee;

					switch (commande) {
					case "CASE_REQUEST" :
						resu = "CASE_RESPONSE";

						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);

						Coordinates coord = new Coordinates(locationX, locationY);

						ArrayList<Coordinates> coorPoss = gameJouee.getPossibleMoves(this.id, coord);
						for (Coordinates c : coorPoss) {
							resu = resu + " " + c.x + " " + c.y;
						}

						if (!gameJouee.legalMove(this)) {
							resu = "PAS_TOUR";
						}
						
						output.println(resu);
						break;

					case "MOVE_REQUEST" :
						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);

						locationX2 = Integer.parseInt(splited[3]);
						locationY2 = Integer.parseInt(splited[4]);

						coordDepart = new Coordinates(locationX, locationY);
						coordArrivee = new Coordinates(locationX2, locationY2);

						if (gameJouee.legalMove(this)) {
							if (gameJouee.askMove(this.id, coordDepart, coordArrivee)) {
								resu = "MOVE_ACCEPTED " + locationX + " " + locationY + " " + locationX2 + " " + locationY2;

								gameJouee.returnCurrentOpponent().output.println("OPPONENT_MOVE " + resu);

							}
							else {
								resu = "MOVE_REFUSED";
							}
						}
						else {
							resu = "PAS_TOUR";
						}

						output.println(resu);
						break;

					case "ACTION_REQUEST" :
						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);
						resu = "ACTION_RESULT";

						coordDepart = new Coordinates(locationX, locationY);
						if (gameJouee.legalMove(this)) {
							ArrayList<String> actionsPoss = new ArrayList<String>();

							actionsPoss = gameJouee.getPossibleActions(this.id, coordDepart);


							for (String s : actionsPoss) {
								resu = resu + " " + s;
							}
						}
						
						if (!gameJouee.legalMove(this)) {
							resu = "PAS_TOUR";
						}

						output.println(resu);
						break;

					case "ATTACK_POSSIBLE" :
						ArrayList<Pair<Coordinates, Integer>> rangeAttack = new ArrayList<Pair<Coordinates, Integer>>();
						splited = command.split("\\s+");
						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);
						coordDepart = new Coordinates(locationX, locationY);
						 
						rangeAttack = gameJouee.getPossibleAttacks(this.id, coordDepart);
						
						resu = "ATTACK_RANGE ";
						
						for (Pair<Coordinates, Integer> c : rangeAttack) {
							resu = resu + " " + c.x.x + " " + c.x.y;
						}
						
						output.println(resu);
						break;
						
					case "ATTACK_REQUEST" :
						splited = command.split("\\s+");
						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);

						locationX2 = Integer.parseInt(splited[3]);
						locationY2 = Integer.parseInt(splited[4]);

						coordDepart = new Coordinates(locationX, locationY);
						coordArrivee = new Coordinates(locationX2, locationY2);

						resu = "RESULT_ATTACK ";
						if (gameJouee.askAttack(this.id, coordDepart, coordArrivee)) {
							if (gameJouee.askPv(coordDepart) > 0) {
								resu += "YES" + " " + locationX + " " + locationY + " ";
							}
							else {
								resu += "NO" + " " + locationX + " " + locationY + " ";
							}
							if (gameJouee.askPv(coordArrivee) > 0) {
								resu += "YES" + " " + locationX2 + " " + locationY2;
							}
							else {
								resu += "NO" + " " + locationX2 + " " + locationY2;
							}

							gameJouee.returnCurrentOpponent().output.println("OPPONENT_MOVE " + resu);
						}

						output.println(resu);
						break;

					case "UNIT_LIST" :
						ArrayList<Pair<String, Integer>> listUnit = new ArrayList<Pair<String, Integer>>();
						resu = "UNIT_RESULT";
						splited = command.split("\\s+");
						
						listUnit = gameJouee.getPossibleUnitCreation(this.id);
						for (Pair<String, Integer> c : listUnit) {
							resu = resu + " " + c.x + " " + c.y;
						}
						
						if (!gameJouee.legalMove(this)) {
							resu = "PAS_TOUR";
						}
						
						output.println(resu);
						break;
						
					case "BUILD_REQUEST" :
						splited = command.split("\\s+");
						String nomUnit = splited[1];
						locationX = Integer.parseInt(splited[2]);
						locationY = Integer.parseInt(splited[3]);
						coord = new Coordinates(locationX, locationY);
						String resultat = gameJouee.askCreateUnit(this.id, coord, nomUnit);
						// String resultat = gameJouee.askBuild(this.id, nomUnit, coord);

						if(resultat != null) {
							resu = "BUILD_RESULT " + resultat;
							
							gameJouee.returnCurrentOpponent().output.println("OPPONENT_MOVE " + resu + " " + locationX + " " + locationY);
					
						}
						else {
							resu = "BUILD_RESULT ";
						}

						output.println(resu);
						break;

					case "CAPTURE_REQUEST" :
						break;

					case "ENDTURN_REQUEST" :
						resu = "";
						if(gameJouee.endTurn(this.id)) {
							resu = "ENDTURN_TRUE";
							
							gameJouee.currentPlayer.output.println("OPPONENT_MOVE " + resu);
						}
					
						else resu = "ENDTURN_FALSE";

						output.println(resu);
						break;
						
					case "QUIT" :
						output.println("KERNEVEZ");
						return;

					default:
						break;

					}
				}

			}
		} catch (IOException e) {
			System.out.println("Player died: " + e);
		} finally {
			try {socket.close();} catch (IOException e) {}
		}
	}
}