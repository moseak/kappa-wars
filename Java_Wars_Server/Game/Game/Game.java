package Game;


import java.awt.Desktop.Action;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import Constants.Actions;
import Constants.CaseTypes;
import Constants.LoggingLevel;
import Constants.UnitTypes;
import Logs.LoggerSingleton;
import Map.Case;
import Map.CaseFactory;
import Map.CaseFactory_MapFile;
import Map.Map;
import Map.MapFactory;
import Map.MapFactory_MapFile;
import Units.Unit;
import Units.UnitFactory;
import other.Coordinates;
import other.Pair;
import Map.Building; 

/**
 * class for
 * @author j_fon
 */
public class Game {
	public String temp;
	public int turn;
	/**
	 * factory of units
	 */
	private UnitFactory uf;
	/**
	 * factory of cases
	 */
	private CaseFactory cf;
	/**
	 * map of the game
	 */
	private Map map;
	/**
	 * Teams of the game (in order)
	 */
	/**
	 * units on the map
	 */
	private Unit[][] units;
	private ArrayList<Team> teams;
	/*
	 * 
	 */
	public Player player1;
	public Player player2;
	
	public Player currentPlayer;
    /**
     * Appel� par le thread du joueur.
     * Appel� quand un move est tent� par un des joueurs.
     * Teste si le move est possible:
     * si c'est son tour
     * si c'est possible
     * s'il est possible on retourne true
     */
    public synchronized boolean legalMove(Player player) {
        if (player == currentPlayer) {
            return true;
        }
        return false;
    }

	/**
	 * default constructor for game
	 * @param map name of the map file
	 * @throws IOException 
	 */
	public Game(String mapFileName) {
		
		// instanciate unit factory
		uf = new UnitFactory();
		// instanciate case factory
		cf = new CaseFactory_MapFile();
		// create Map
		MapFactory mf = new MapFactory_MapFile();
		this.map = mf.createMapFromFile("..\\maps\\" + mapFileName + ".map");
		this.teams = new ArrayList<Team>();
		turn = 1;
		// ajout des equipes
		for(int i= 0; i< map.nbOfPlayer; i++)
		{
			teams.add(new Team("joueur " + i));
		}
		// set units
		units = new Unit[map.lignes][map.colonnes];
	}
	// ================= LISTE DES FONCTIONS ========================
	/*
	 * 
	 */
	public Player returnCurrentOpponent() {
		if (currentPlayer.id == player1.id) 
			return player2;
		else
			return player1;
	}
	
	public boolean endTurn(int idP) {
		System.out.println("ID JOUEUR DEMANDANT FIN TOUR " + idP);
		if (idP == currentPlayer.id) {
			if (idP == player1.id) {
				currentPlayer = player2;
				endOfTurn();
				return true;
			}
			else if (idP == player2.id) {
				currentPlayer = player1;
				endOfTurn();
				return true;
			}
		}
		return false;
	}
	
	public void setPlayer1(Player player) {
		this.player1 = player;
	}
	public void setPlayer2(Player player) {
		this.player2 = player;
	}
	
	
	/**
	 * @return lignes + " "  + colonnes
	 */
	public String getMapDimensionForClient()
	{
		return map.lignes + " " + map.colonnes;
	}
	
	public String askCapture(int team, Coordinates coord)
	{
		String s = "";
		// unit� controlable
		if(isThereControlableUnit(team, coord))
		{
			Unit u = units[coord.x][coord.y];
			// v�rifier que le batiment est capturable et que l'unit peut l'effectuer
			if(capturable(team, coord) && Rules.canUnitCapture(u.type))
			{
				map.setCaseAt(coord.x, coord.y, cf.createCaseFromString("C" + team) );
				s = map.getCaseAt(coord.x, coord.y).data.spriteName;
			}
		}
		return s;
	}
	
	/**
	 * return array 2d of sprite names
	 */
	public String[][] getMapForClient()
	{
		String[][] spriteNames = new String[map.lignes][map.colonnes];
		for(int i = 0; i < map.lignes; i++)
		{
			for(int j = 0; j < map.colonnes; j++)
			{
				spriteNames[i][j] = map.getCaseAt(i, j).data.spriteName;
			}
		}
		return spriteNames;
	}
	
	/**
	 * cr�e l'unit� de mand� � la bonne case
	 * @param team
	 * @param from
	 * @param unitName
	 * @return le sprite si la cr�ation est ok, null si impossible
	 */
	public String askCreateUnit(int team, Coordinates from, String unitName)
	{
		System.out.println("ID JOUEUR DEMANDANT BUILD " + team);
		String spriteName = null;
		// checker si il ny a pas deja une unite presente
		if(caseFree(from))
		{
			Case c = map.getCaseAt(from.x, from.y);
			// checker si c'est une factory et qu'elle apparteint au bon joueur
			if(c.type == CaseTypes.FACTORY)
			{
				// appartient-il a la bonne equipe
				if ( ((Building)c).team == team )
				{
					// cr�er l'unite
					units[from.x][from.y] = uf.createUnit(unitName, team);
					spriteName = units[from.x][from.y].data.spriteName;
				}
			}
		}
		return spriteName;
	}
	
	/**
	 * end of turn
	 * re allow moves
	 */
	public void endOfTurn()
	{
		Unit u;
		// set all units to have not played
		for(int i=0; i < map.lignes; i++ )
		{
			for(int j=0; j < map.colonnes; j++ )
			{
				u = units[i][j];
				if(u != null)
				{
					u.hasPlayed = false;
					u.hasMoved = false;
				}
			}
		}
		turn ++;
	}
	
	/**
	 * return the list of possible cases movement 
	 * @param team number of the team
	 * @param from case of origin
	 * @return list of coordinates
	 */
	public ArrayList<Coordinates> getPossibleMoves(int team, Coordinates from)
	{
		System.out.println(team + " " + from.x + " " + from.y);
		ArrayList<Coordinates> possibleMoves = new ArrayList<Coordinates>();
		Unit u = units[from.x][from.y];
		// there must be an unit
		if(u != null)
		{
			// if we have control on this unit and it hasn't already played
			if (u.team == team && !u.hasPlayed)
			{
				possibleMoves = computePossibleMoves(u, from);
			}
		}
		return (possibleMoves);
	}
	
	/**
	 * return list of possible actions from a team on a case
	 * @param team
	 * @param coord
	 * @return
	 */
	public ArrayList<String> getPossibleActions(int team, Coordinates coord)
	{
		ArrayList<String> possibleActions = new ArrayList<String>();
		possibleActions.add(Actions.endOfTurn.toString());
		// est ce qu'il y a une unite sur cette case ?
		Unit u = units[coord.x][coord.y];
		if(u != null)
		{
			if(isThereControlableUnit(team, coord))
			{
				// wait
				possibleActions.add(Actions.wait.toString());
				// attack
				if ( !getPossibleAttacks(team, coord).isEmpty() )
				{
					possibleActions.add(Actions.attack.toString());
				}
				// capture
				if( capturable(team,coord) && Rules.canUnitCapture(u.type))
				{
					possibleActions.add(Actions.capture.toString());
				}
			}
		}
		// est ce qu'il y a un batiment qui m'appartient
		else
		{
			Case c = map.getCaseAt(coord.x, coord.y);
			// est-ce un batiment ?
			if(c instanceof Building)
			{
				// est-ce que c'est un batiment de la bonne equipe ?
				if( ((Building)c).team == team )
				{
					// une factory
					if( c.type == CaseTypes.FACTORY )
					{
						possibleActions.add(Actions.createUnit.toString());
					}
				}
			}
		}
		return possibleActions;
	}
	
	/**
	 * capture possible
	 * @param coord
	 * @return
	 */
	private boolean capturable(int team, Coordinates coord) {
		boolean captureable = false;
		// est ce qu'il y a un batiment ?
		Case c = map.getCaseAt(coord.x, coord.y);
		if( c instanceof Building )
		{
			captureable = (  ((Building)c).team != team );
		}
		return captureable;
	}

	/**
	 * nom des unit�s qu'il est possible de cr�er
	 * @param team
	 * @return
	 */
	public ArrayList<Pair<String, Integer>> getPossibleUnitCreation(int team)
	{
		Pair<String, Integer> info;
		ArrayList<Pair<String, Integer>> creation = new ArrayList<Pair<String, Integer>>();
		
		Unit u = uf.createUnit(UnitTypes.infantry.toString(), team);
		info = new Pair<String, Integer>(u.type.toString(), u.data.price);
		creation.add(info);
		u = uf.createUnit(UnitTypes.recoon.toString(), team);
		info = new Pair<String, Integer>(u.type.toString(), u.data.price);
		creation.add(info);
		u = uf.createUnit(UnitTypes.tank.toString(), team);
		info = new Pair<String, Integer>(u.type.toString(), u.data.price);
		creation.add(info);
		u = uf.createUnit(UnitTypes.mech.toString(), team);
		info = new Pair<String, Integer>(u.type.toString(), u.data.price);
		creation.add(info);
		
		return creation;
	}
	
	/**
	 * ask a movement
	 * @param team
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean askMove(int team, Coordinates from, Coordinates to)
	{
		boolean done = false;
		
		// is the move in possible moving cases ?
		ArrayList<Coordinates> possibleDeps = getPossibleMoves(team,  from);
		boolean in = false;
		for(Coordinates c : possibleDeps)
		{
			if(c.x == to.x && c.y == to.y)
			{
				in = true;
				break;
			}
		}
		if(!in) return false;
		
		Unit u = units[from.x][from.y];
		// is there an unit
		if(u != null)
		{
			// is the move possible
			if ( isPossibleMove(u, from, to) )
			{
				// change the case of the unit
				units[from.x][from.y] = null;
				units[to.x][to.y] = u;
				u.hasMoved = true;
				done = true;
			}
 		}
		return done;
	}
	
	/**
	 * get possible attacks of the unit + estimates damage on it
	 * @param team
	 * @param coord
	 * @return
	 */
	public ArrayList<Pair<Coordinates, Integer>> getPossibleAttacks(int team, Coordinates coord)
	{
		ArrayList<Pair<Coordinates, Integer>> attacks = new ArrayList<Pair<Coordinates, Integer>>();
		
		// y'a til une unit� � controller ?
		if(isThereControlableUnit(team, coord))
		{
			Coordinates[] cs = new Coordinates[4];
			cs[0] = new Coordinates(coord.x, coord.y+1);
			cs[1] = new Coordinates(coord.x, coord.y-1);
			cs[2] = new Coordinates(coord.x-1, coord.y);
			cs[3] = new Coordinates(coord.x+1, coord.y);
			// est ce qu'il y a une unit� a attaquer � ces coordonn�es 
			ArrayList<Coordinates> attaquesPOssibles = new ArrayList<Coordinates>();
			Unit uToTaccak;
			for(int i=0; i<4; i++)
			{
				// coords in map
				if(map.inMap(cs[i]))
				{
					uToTaccak = units[cs[i].x][cs[i].y];
					if(uToTaccak != null)
					{
						if (uToTaccak.team != team)
						{
							attaquesPOssibles.add(cs[i]);
						}
					}
				}
			}
			// pour toutes les attaques possibles, calculer les d�gats
			for(Coordinates c : attaquesPOssibles)
			{
				attacks.add( new Pair<Coordinates,Integer>(c,computeDamages(coord, c)) );
			}
		}
		return attacks;
	}
	
	/**
	 * action
	 * dire a l'unite d'attendre
	 * fin des actions pour l'unite
	 * @param team
	 * @param from
	 */
	public void wait(int team, Coordinates from)
	{
		if( isThereControlableUnit(team,from) )
		{
			Unit u = units[from.x][from.y];
			u.hasMoved = true;
			u.hasPlayed = true;
		}
	}
	
	/**
	 * as for an attack
	 * @param team
	 * @param from
	 * @param to
	 * @return true if done, false if impossible
	 */
	public boolean askAttack(int team, Coordinates from, Coordinates to)
	{
		boolean attackPossible = false;
		
		// v�rifier si l'attaque demand�e fait partie des attaques possibles;
		ArrayList<Pair<Coordinates, Integer>> listOfAttacks = getPossibleAttacks(team, from);
		boolean coordonneesPossibles = false;
		for(Pair<Coordinates, Integer> coord : listOfAttacks)
		{
			if(coord.x.x == to.x && coord.x.y == to.y)
			{
				coordonneesPossibles = true;
				break;
			}
		}
		if(coordonneesPossibles)
		{
			Unit u1, u2;
			u1 = units[from.x][from.y];
			u2 = units[to.x][to.y];
			// attaque de a vers b;
			int dmg = computeDamages(from, to);
			// supression des points de vie de l'unit� ennemie
			u2.data.pv -= dmg;
			// si l'unit� est en vie, riposter
			// attaque de b vers a;
			dmg = computeDamages(to, from);
			// supression des points de vie de l'unit� ennemie
			u1.data.pv -= dmg;
			// supprimer les unit�s mortes
			if(u1.data.pv <= 0)
			{
				units[from.x][from.y] = null;
			}
			if(u2.data.pv <= 0)
			{
				units[to.x][to.y] = null;
			}
			attackPossible = true;
		}
		return attackPossible;
	}
	
	/**
	 * indicates the number of pvs from a unit on a case
	 * @return number of hp, 0 if no unit here
	 */
	public int askPv(Coordinates coord)
	{
		Unit u = units[coord.x][coord.y];
		if(u != null)
		{
			return u.getNumberOf();
		}
		return 0;
	}
	
	// ========================= ALGORITHMES ========================================
	/**
	 * compute damages dealt from an unit to an other
	 * according to environment protection
	 * @param from
	 * @param to
	 * @return -1 if error
	 */
	private int computeDamages(Coordinates from, Coordinates to)
	{
		int dmg = -1;
		// get 2 units
		Unit unit1 = units[from.x][from.y];
		Unit unit2 = units[to.x][to.y];
		// get protection
		int p = map.getCaseAt(to.x, to.y).data.protection;
		if(unit1 != null && unit2 != null)
		{
			// damage dealt by unit1 on unit 2
			dmg = unit1.data.degats.get(unit2.type);
			// multiply by the number of unit
			dmg *= unit1.getNumberOf();
			// reduction granted by environment
			dmg *= 10 - p;
			dmg /= 10;
		}
		return dmg;
	}
	
	/**
	 * compute possibles moves of an unit from a position
	 * @param u
	 * @param from
	 * @return
	 */
	private ArrayList<Coordinates> computePossibleMoves(Unit u, Coordinates from)
	{
		ArrayList<Coordinates> possibleMoves = new ArrayList<Coordinates>();
		ArrayList<Coordinates> unitMoves = computeMovementCases(u, from);
		// is there a possible move on each case
		for(Coordinates unitMove : unitMoves)
		{
			if(isPossibleMove(u,from,unitMove))
			{
				possibleMoves.add(unitMove);
			}
		}
		return possibleMoves;
	}
	
	/**
	 * indicates if the defined move from this unit is possible in the map
	 * @param u
	 * @param from
	 * @param to
	 * @return
	 */
	private boolean isPossibleMove(Unit u, Coordinates from, Coordinates to)
	{
		// not out of map
		if (!map.inMap(to))
		{
			return false;
		}
		// test if accessible
		if( !Rules.canUnitGoOn( u.type, map.getCaseAt(to.x, to.y).type ) )
		{
			return false;
		}
		// test if not alrdeady an unit on it
		if( !from.compare(to) && units[to.x][to.y] != null)
		{
			return false;
		}
		// if the unit hasn't already played
		if(u.hasPlayed)
		{
			return false;
		}
		// if the unit hasn't already played
		if(u.hasMoved)
		{
			return false;
		}
		// else the deplacement is possible
		return true;
	}

	/**
	 * all movements possible for this unit
	 * @param u
	 * @param from
	 * @return
	 */
	private ArrayList<Coordinates> computeMovementCases(Unit u, Coordinates from)
	{
		// manage single values
		java.util.Map<String, Coordinates> membresMap = new HashMap() ;
		ArrayList<Coordinates> coords = new ArrayList<Coordinates>();
		Coordinates temp;
		for( int x = 0; x <= u.data.moveRange; x++ )
		{
			for( int y = 0; y<= u.data.moveRange - x; y++ )
			{
				temp = new Coordinates(from.x + x, from.y + y);
				membresMap.put(new String(temp.x + " " + temp.y) , temp);
				temp = new Coordinates(from.x - x, from.y - y);
				membresMap.put(new String(temp.x + " " + temp.y) , temp);
				temp = new Coordinates(from.x - x, from.y + y);
				membresMap.put(new String(temp.x + " " + temp.y) , temp);
				temp = new Coordinates(from.x + x, from.y - y);
				membresMap.put(new String(temp.x + " " + temp.y) , temp);
			}
		}
		// add MappedValues to array list
		Set listKeys=membresMap.keySet();  // Obtenir la liste des cl�s
		Iterator iterateur=listKeys.iterator();
		// Parcourir les cl�s et afficher les entr�es de chaque cl�;
		while(iterateur.hasNext())
		{
			Object key= iterateur.next();
			coords.add(membresMap.get(key));
		}
		return coords;
	}
	
	/**
	 * tell if there is a unite on the map at this coordinate
	 * @param c
	 * @return true / false
	 */
	private boolean caseFree(Coordinates c)
	{
		return (units[c.x][c.y] == null);
	}
	
	/**
	 * indicates if there is a controlable unit on the case
	 * @param team
	 * @param coord
	 */
	private boolean isThereControlableUnit(int team, Coordinates coord)
	{
		Unit u = units[coord.x][coord.y];
		// is there an unit
		if(u == null) { return false; }
		// is it good team
		if(u.team != team) { return false; }
		// hasnt it already played
		if(u.hasPlayed) { return false; }
		return true;
	}
	
	public static void main(String[] agrs)
	{
		// variables
		ArrayList<String> possibleActions;
		ArrayList<Pair<String, Integer>> listOfUnitsToCreate;
		
		// cr�ation de la partie sur la map ile haricot
		Game myGame = new Game("ile_haricot"); // test OK
		
		// demande des dimensions de la carte par le client
		String dimensions = myGame.getMapDimensionForClient(); // test OK
		// demande de la carte par le client
		String[][] spriteNames = myGame.getMapForClient(); // test OK
		
		// test d'affichage de la carte retourn�e // test OK
		/*
		for(int i=0; i< Integer.parseInt( myGame.getMapDimensionForClient().split(" ")[0] ) ; i++)
		{
			for(int j=0; j< Integer.parseInt( myGame.getMapDimensionForClient().split(" ")[1] ) ; j++)
			{
				System.out.print(spriteNames[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		}
		*/
		System.out.println("Tour " + myGame.turn);
		
		// demande les actions possible sur l'usine en 6 1
		possibleActions = myGame.getPossibleActions(1, new Coordinates(6,1));
		// affaichage des actions possibles
		System.out.println("Actions possible en 6 1 :");
		for(String s : possibleActions)
		{
			System.out.println(s);
		}
		
		//demande des unites possibles a creer
		listOfUnitsToCreate = myGame.getPossibleUnitCreation(1);
		System.out.println("Liste des unit�s qu'il est possible de cr�er : ");
		for(Pair<String,Integer> p : listOfUnitsToCreate)
		{
			System.out.println(p.x + " : " + p.y);
		}
		
		// La team 1 demande la cr�ation d'un infantry sur la case 6 1
		// affiche le nom du sprite cr��
		System.out.println("Nom du sprite unit� cr�� : " + myGame.askCreateUnit(1, new Coordinates(6, 1), UnitTypes.infantry.toString()) ); // test OK
		
		// nouveau tour
		myGame.endOfTurn(); // test OK
		System.out.println("Tour " + myGame.turn);
		
		// La team 1 demande les d�placements possible sur la case  6 1
		ArrayList<Coordinates> possibleDeps = myGame.getPossibleMoves(1,  new Coordinates(6, 1)); // test OK
		// affichage des coordon�es retourn�es
		System.out.println("D�placements possibles depuis 6 1 :");
		for(Coordinates c : possibleDeps)
		{
			System.out.println(c.x + " " + c.y);
		}

		// d�placement
		System.out.println("D�placement de linfantry de 6 1 vers 7 1");
		myGame.askMove(1, new Coordinates(6,1), new Coordinates(7,1)); // testOK
		
		possibleDeps = myGame.getPossibleMoves(1,  new Coordinates(7, 1)); // test OK
		// affichage des coordon�es retourn�es
		System.out.println("D�placements possibles depuis 7 1 :");
		for(Coordinates c : possibleDeps)
		{
			System.out.println(c.x + " " + c.y);
		}
		
		// nouveau tour
		myGame.endOfTurn();
		System.out.println("Tour " + myGame.turn);
		
		// cr�ation non conventionelle d'un ennemi en case 7 2
		UnitFactory uf = new UnitFactory();
		myGame.units[7][2] = uf.createUnit(UnitTypes.recoon.toString(), 2);
		
		// demande des actions possibles depuis l'infantry 7 1
		possibleActions = myGame.getPossibleActions(1, new Coordinates(7,1));
		// affaichage des actions possibles
		System.out.println("Actions possible en 7 1 (infantry):");
		for(String s : possibleActions)
		{
			System.out.println(s);
		}
		
		// demander la liste des attaques
		ArrayList<Pair<Coordinates, Integer>> listOfAttacks = myGame.getPossibleAttacks(1, new Coordinates(7, 1));
		System.out.println("Liste des attaques possibles de l'infanterie 7 1 :");
		// les afficher
		for( Pair<Coordinates, Integer> attack :listOfAttacks )
		{
			System.out.println("x : "+ attack.x.x + ", y : "+ attack.x.y + ", degats etimes : " + attack.y);
		}
		
		// attaque de l'infantry 71 vers le recoon 72
		boolean attackPossible = myGame.askAttack(1, new Coordinates(7, 1), new Coordinates(7, 2));
		System.out.println(attackPossible);
	}
}