package Game;

import java.util.ArrayList;

import Units.Unit;

/**
 * team of a game
 * @author j_fon
 */
public class Team {
	/**
	 * Name of the team
	 */
	public String name;
	/**
	 * Default constructor for team
	 * @param name name of the team
	 */
	public Team(String name)
	{
		this.name = name;
	}
}
