package Game;

import Constants.CaseTypes;
import Constants.Colors;
import Constants.UnitTypes;
import Units.Unit;

public abstract class Rules {
	public static boolean canUnitGoOn(UnitTypes ut, CaseTypes ct)
	{
		// only people on mountain
		if(ct == CaseTypes.MOUNTAIN && (ut != UnitTypes.infantry || ut != UnitTypes.mech))
		{
			return false;
		}
		// no unit on water
		if(ct == CaseTypes.WATER)
		{
			return false;
		}
		// then its ok
		return true;
	}
	
	public static Colors getTeamColor(int team)
	{
		Colors c;
		switch(team)
		{	
			case 1 :
				c = Colors.red;
				break;
			case 2 :
				c = Colors.blue;
				break;
			case 3 :
				c = Colors.green;
				break;
			case 4 :
				c = Colors.yellow;
				break;
			default :
				c = Colors.white;
		}
		return c;
	}
	
	public static boolean canUnitCapture(UnitTypes ut)
	{
		return (ut == UnitTypes.infantry || ut == UnitTypes.mech);
	}
}
