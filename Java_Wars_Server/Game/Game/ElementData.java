package Game;

/**
 * common datas of elemnts
 * @author j_fon
 *
 */
public abstract class ElementData {
	/**
	 * Name of the element
	 */
	public String name;
	/**
	 * Description of the element 
	 */
	public String desc;
	/**
	 * Adres of the sprtie's image
	 */
	public String spriteName;
	/**
	 * Constructor for element datas
	 */
	public ElementData()
	{
		this.spriteName = new String();
		this.name = new String();
		this.desc = new String();
	}
	/**
	 * @return Description as String
	 */
	public abstract String getDescription();
}
