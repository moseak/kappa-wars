package Game;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;


import Constants.UnitTypes;
import other.Coordinates;

public class EXE_Serveur {
	public static void main(String[] agrs) throws IOException
	{
		ServerSocket listener = new ServerSocket(8901);
		System.out.println("Le server se lance");
		
		try {
			// cr�ation de la partie
			Game myGame = new Game("ile_haricot"); // test OK
			myGame.temp = "coucou";
	        Player player1 = new Player(listener.accept(), 1, myGame);
	        System.out.println("Joueur 1 connecte");
	        Player player2 = new Player(listener.accept(), 2, myGame);
	        System.out.println("Joueur 2 connecte");
	        myGame.setPlayer1(player1);
	        myGame.setPlayer2(player2);
	        
	        System.out.println(player1.gameJouee.temp);
	        player1.gameJouee.temp = "nouveau coucou";
	        
	        System.out.println(myGame.temp);
	        
	        myGame.currentPlayer = player1;
	        
	        player1.start();
	        player2.start();

		
	    } finally {
	        listener.close();
	    }

	}
    
}
