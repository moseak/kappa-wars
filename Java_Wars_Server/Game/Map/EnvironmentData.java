package Map;

/**
 * structure describing datas of an Environment
 * @author j_fon
 */
public class EnvironmentData extends CaseData {
	/**
	 * Default constructor for EnvironmentData
	 */
	public EnvironmentData() {
		super();
	}
	
	public String getHTMLDescription() {
		String formatedDescription;
		formatedDescription  = "<html>";
		formatedDescription += this.name + "<br>";
		formatedDescription += this.desc + "<br>";
		formatedDescription += "Protection : " + this.protection + "<br>";
		formatedDescription += "</html>";
		return formatedDescription;
	}
	@Override
	public String getDescription() {
		String formatedDescription;
		formatedDescription  = this.name + "\n";
		if(! desc.equals(""))
		{
			formatedDescription += this.desc + "\n";
		}
		formatedDescription += "Protection : " + this.protection + "\n";
		formatedDescription += "\n";
		return formatedDescription;
	}
}
