package Map;

import Constants.CaseTypes;
import Constants.Colors;

public class Building extends Case {
	/**
	 * team number of the team
	 * 0 : neutral
	 * 1-4 : players
	 */
	public int team;
	/**
	 * updates the color of the sprite
	 * @param c
	 */
	public void updateColor(Colors c)
	{
		// TODO modifier une coleur
	}
	/**
	 * Constructor for a building
	 * @param bd datas describing the building
	 * @param team number of the team (0 : neutral, 1-4 players)
	 */
	public Building(CaseTypes type, BuildingData bd, int team)
	{
		this.type = type;
		this.data = bd;
		this.team = team;
	}
}
