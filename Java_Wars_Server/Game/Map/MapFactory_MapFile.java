package Map;

import Data.BasicTxtParser;
import Data.GameDataProvider;
import Data.GameDataProvider_Static;
import other.Grid2D;

/**
 * Map creator based on text "*.map" text files
 * @author j_fon
 *
 */
public class MapFactory_MapFile implements MapFactory {
	/**
	 * Parser for basic txt file
	 */
	BasicTxtParser parser;
	/**
	 * Constructor for MapFactory_MapFile
	 */
	public MapFactory_MapFile()
	{
		// instanciation of parser
		parser = new BasicTxtParser();
	}
	
	// TODO cr�er la map a partir du fichier texts
	@Override
	public Map createMapFromFile(String filePath)
	{
		// open file
		if ( parser.openFile(filePath) )
		{
			// 1st line is the name of the map
			String map_name = parser.nextLine();
			// 2nd line for number of players
			int nbPlayers = Integer.parseInt(parser.nextLine());
			// 3r line is for dimensions (x y)
			String temp = parser.nextLine();
			String[] tempSplit = temp.split(" ");
			int lignes, colonnes;
			colonnes = Integer.parseInt(tempSplit[0]);
			lignes = Integer.parseInt(tempSplit[1]);
			// rest of file is the 2DGrid
			Case[][] cases = new Case[lignes][colonnes];
			CaseFactory cf = new CaseFactory_MapFile();
			// read each line of the map
			for(int i=0; i<lignes; i++)
			{
				temp = parser.nextLine();
				tempSplit = temp.split("\\s+");
				// for each case
				for(int j=0; j<colonnes; j++ )
				{
					cases[i][j] = cf.createCaseFromString(tempSplit[j]);
				}
			}
			return ( new Map(map_name, nbPlayers, lignes, colonnes, cases ) ) ;
		}
		return null;
	}
}
