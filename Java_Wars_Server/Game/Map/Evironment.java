package Map;

import Constants.CaseTypes;

/**
 * Natural environment
 * @author j_fon
 */
public class Evironment extends Case {
	/**
	 * Constructor for environment
	 * @param type type of case
	 * @param ed data describing the environment
	 */
	public Evironment(CaseTypes type, EnvironmentData ed)
	{
		this.type = type;
		this.data = ed;
	}
}
