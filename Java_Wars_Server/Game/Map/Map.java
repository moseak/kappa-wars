package Map;

import other.Coordinates;
import other.Grid2D;

/**
 * map of the game
 * @author j_fon
 */
public class Map {
	public int nbOfPlayer;
	/**
	 *  map's name
	 */
	public String name;
	/**
	 *  number of lines
	 */
	public int lignes;
	/**
	 *  number of columns
	 */
	public int colonnes;
	/**
	 *  2D array of cases
	 */
	private Case[][] environmentArray;
	/**
	 * 
	 * @param name
	 * @param nbOfPlayers
	 * @param lignes
	 * @param colonnes
	 * @param cases
	 */
	public Map(String name, int nbOfPlayers, int lignes, int colonnes, Case[][] environmentArray)
	{
		// set attributes
		this.name = name;
		this.lignes = lignes;
		this.colonnes = colonnes;
		this.nbOfPlayer = nbOfPlayers;
		this.environmentArray = environmentArray;
	}
	/**
	 * gives the name of the map
	 * @return map name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * indicates if the coordinates are in the map
	 * @param c line,column
	 * @return
	 */
	public boolean inMap(Coordinates c)
	{
		return (c.x<this.lignes  && c.y<this.colonnes && c.x>=0 && c.y>=0);
	}
	/**
	 * get case a indicated coordonates
	 * @param  ligne
	 * @param  colonne
	 * @return
	 */
	public Case getCaseAt(int ligne, int colonne)
	{
		return environmentArray[ligne][colonne];
	}
	
	public void setCaseAt(int ligne, int colonne, Case c)
	{
		environmentArray[ligne][colonne] = c;
	}
}
