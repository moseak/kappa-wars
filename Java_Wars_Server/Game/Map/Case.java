package Map;

import Constants.CaseTypes;
import Game.Element;

/**
 * case of a map
 * @author j_fon
 */
public abstract class Case implements Element {
	public CaseData data;
	/**
	 * Type of the case
	 */
	public CaseTypes type;
}
