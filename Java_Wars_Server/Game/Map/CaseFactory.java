package Map;

/**
 * Provides cases from row data
 * @author j_fon
 */
public interface CaseFactory {
	/**
	 * creates a case based on description description string
	 * @param desc string describing the case
	 * @return the case
	 */
	public Case createCaseFromString(String desc);
	/**
	 * creates a case based on description description string
	 * if the case is a Building create it and returns it
	 * if the case is an environment, create it once, then returns single reference to it
	 * @param desc string describing the case
	 * @return the case
	 */
	public Case getCaseFromString(String desc);
}
