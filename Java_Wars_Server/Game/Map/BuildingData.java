package Map;
/**
 * structure describing datas of a building
 * @author j_fon
 */
public class BuildingData extends CaseData {
	/**
	 * range of vision granted by this building
	 */
	public int visionRange;
	/**
	 * income granted by turn
	 */
	public int income;
	/**
	 * Default constructor for Building Data
	 */
	public BuildingData() {
		super();
		visionRange = 0;
		income = 0;
	}
	public String getHTMLDescription() {
		String formatedDescription;
		formatedDescription  = "<html>";
		formatedDescription += this.name + "<br>";
		if(desc != "")
		{
			formatedDescription += this.desc + "<br>";
		}
		if(income > 0)
		{
			formatedDescription += "Income : " + this.income + "<br>";
		}
		if(visionRange > 0)
		{
			formatedDescription += "Vision : " + this.visionRange + "<br>";
		}
		formatedDescription += "Protection : " + this.protection + "<br>";
		formatedDescription  = "</html>";
		return formatedDescription;
	}
	@Override
	public String getDescription() {
		String formatedDescription;
		formatedDescription = this.name + "\n";
		if(!desc.equals(""))
		{
			formatedDescription += this.desc + "\n";
		}
		if(income > 0)
		{
			formatedDescription += "Income : " + this.income + "\n";
		}
		if(visionRange > 0)
		{
			formatedDescription += "Vision : " + this.visionRange + "\n";
		}
		formatedDescription += "Protection : " + this.protection + "\n";
		return formatedDescription;
	}
	public BuildingData copy()
	{
		BuildingData bd = new BuildingData();
		bd.name = this.name;
		bd.desc = this.desc;
		bd.income = this.income;
		bd.visionRange = this.visionRange;
		bd.protection = this.protection;
		bd.spriteName = this.spriteName;
		
		return bd;
	}
}
