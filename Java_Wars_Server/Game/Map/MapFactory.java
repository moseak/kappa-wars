package Map;

/**
 * Provides a map objet from row data
 * @author j_fon
 *
 */
public interface MapFactory {
	/**
	 * Create a Map based on a description file
	 * @param filePath
	 * @return the created Map, null if creation failed
	 */
	public Map createMapFromFile(String filePath);
}
