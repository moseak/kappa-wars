package Map;

import Constants.CaseTypes;
import Constants.Colors;
import Data.GameDataProvider;
import Data.GameDataProvider_BDD;
import Data.GameDataProvider_Static;
import Data.MapCaseTranslator;
import Game.Rules;

/**
 * Provides cases from row data
 * @author j_fon
 */
public class CaseFactory_MapFile implements CaseFactory{
	/**
	 * provider of game data
	 */
	private GameDataProvider gdp;
	/**
	 * Constructor for case facory based on map files information
	 */
	public CaseFactory_MapFile()
	{
		// static data provider to begin with
		gdp = new GameDataProvider_BDD();
	}
	/**
	 * creates a case base on description *.map description string
	 * @param desc string describing the case
	 * @return the case
	 */
	public Case createCaseFromString(String desc)
	{
		Case c;
		CaseTypes type = MapCaseTranslator.translate(desc.substring(0,1));
		// is it a building
		if( isBuilding( type ) )
		{
			// char of the building
			BuildingData bd = gdp.getBuildingData(type);
			// number of the team
			int team = Integer.parseInt(desc.substring(1));
			// color of the team
			setColor(team, bd);
			c = new Building(type, bd, team);
		}
		// or environment
		else
		{
			// char of the environment
			EnvironmentData ed = gdp.getEnvironmentData(type);
			c = new Evironment(type,ed);
		}
		c.type = type;
		return c;
	}
	
	/**
	 * idicates if the given string corresponds to a building
	 * @param s map case string
	 * @return true if building, false if environment
	 */
	private boolean isBuilding(CaseTypes c)
	{
		return ( c == CaseTypes.FACTORY  || c == CaseTypes.CAPITAL || c == CaseTypes.CITY );
	}
	/**
	 * changes the color of the spire in function of the team
	 * @param teamNumber
	 * @param bd
	 */
	private void setColor(int teamNumber, BuildingData bd)
	{
		String colorName = Rules.getTeamColor(teamNumber).toString();
		bd.spriteName = bd.spriteName.replace("color", colorName);
	}
	@Override
	public Case getCaseFromString(String desc) {
		// TODO Auto-generated method stub
		return null;
	}

}
