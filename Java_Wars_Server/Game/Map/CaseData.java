package Map;

import Game.ElementData;

/**
 * structure describing datas of a case
 * @author j_fon
 */
public abstract class CaseData extends ElementData{
	/**
	 * protection granted by this type of case
	 * from 0 to 5
	 */
	public int protection;
	/**
	 * Default constructor for case data
	 */
	public CaseData()
	{
		protection = 0;
	}
}
