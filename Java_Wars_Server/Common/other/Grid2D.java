package other;

import java.util.ArrayList;

import Constants.LoggingLevel;
import Logs.LoggerSingleton;

/**
 * 2D array of T
 * @author j_fon
 * @param <T> type of template
 */
public class Grid2D<T> {
	/**
	 * x number of columns
	 */
	public int dimX;
	/**
	 * y number of row
	 */
	public int dimY;
	/**
	 * double array list for dynamic 2D table
	 * contains T
	 */
	private ArrayList<ArrayList<T>> grid;
	/**
	 * Build a Grid2D of x per y T elements
	 * @param x column dimension
	 * @param y row dimension
	 */
	public Grid2D(int x, int y)
	{
		dimX = x;
		dimY = y;
		// instanciation of the grid
		grid = new ArrayList<ArrayList<T>>();
		// initialisation of the grid
		for(int i = 0; i < y; i ++)
		{
			grid.add(new ArrayList<T>());
		}
	}
	/**
	 * returns the value of the case [x,y]
	 * @param x column
	 * @param y row
	 * @return T value of the case, null if out of range
	 */
	public T get(int x, int y)
	{
		// test cordinates
		if (valideCoordinates(x, y))
		{	
			return grid.get(y).get(x);
		}
		else
		{
			LoggerSingleton.log(LoggingLevel.ERROR, getClass().getName(), "getting coordonates out of dimension");
		}
		return null;
	}
	/**
	 * ad value of an element on the 2D grid
	 * @param x column (osef)
	 * @param y row
	 * @param value T value
	 * @return true if correctly set, false if not
	 */
	public boolean add(int y, int x, T value)
	{
		// test cordinates
		if (valideCoordinates(x, y))
		{	
			grid.get(y).add(value);
			return true;
		}
		else
		{
			LoggerSingleton.log(LoggingLevel.ERROR, getClass().getName(), "setting coordonates out of dimension");
		}
		return false;
	}
	/**
	 * tells if the given coordinates are valid in this grid
	 * @param x x_case
	 * @param y y_case
	 * @return true if coordinates exsits in this grid
	 */
	private boolean valideCoordinates(int x, int y)
	{
		return (x<this.dimX  && y<this.dimY && x>=0 && y>=0);
	}
}
