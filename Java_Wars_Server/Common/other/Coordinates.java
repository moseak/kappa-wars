package other;

/**
 * coordinates of a case
 * @author j_fon
 */
public class Coordinates {
	/**
	 * horizontal
	 */
	public int x;
	/**
	 * vertical
	 */
	public int y;
	
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}	
	/**
	 * tells is coordinates c2 is same than current
	 * @param c2
	 * @return true if same, false if else
	 */
	public boolean compare(Coordinates c2)
	{
		return (x == c2.x && y == c2.y);
	}
}
