package Logs;

import Constants.LoggingLevel;

/**
 * logger for game information
 * @author j_fon
 *
 */
public class LoggerSingleton {
	/**
	 * instance of the logger
	 */
	//private static LoggerSingleton myInstance;
	/**
	 * (generates  and gives )or (gives) the instance of the singleton
	 * May not be thread safe
	 * @return instance of logger singleton
	 */
	/*
	public static LoggerSingleton getInstance()
	{
		if (myInstance == null)
		{
			myInstance = new LoggerSingleton();
		}
		return myInstance;
	}
	*/
	/**
	 * Logs and error in the defined canal
	 * @param message message you want to log
	 */
	public static void log(LoggingLevel lvl,String objectName , String message)
	{
		System.out.println("[" + lvl.toString() + "] from [" + objectName + "]" + " : [" + message + "]");
	}
}
