package Constants;

import Logs.LoggerSingleton;

public class StringToEnum {
	public static UnitTypes getUnitType(String s)
	{
		
		switch (s)
		{
			case "infantry" :
				return UnitTypes.infantry;
			case "mech" :
				return UnitTypes.mech;
			case "tank" :
				return UnitTypes.tank;
			case "tank_m" :
				return UnitTypes.tank_m;
			case "dca" :
				return UnitTypes.dca;
			case "recoon" :
				return UnitTypes.recoon;
			default :
				LoggerSingleton.log(LoggingLevel.ERROR, "StringToEnum", "Impossible to get unit : " + s);
				return null;
		}
	}
}
