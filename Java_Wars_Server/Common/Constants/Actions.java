package Constants;

public enum Actions {
	createUnit,
	endOfTurn,
	wait,
	attack,
	capture
}
