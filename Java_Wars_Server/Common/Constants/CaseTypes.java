package Constants;

public enum CaseTypes {
	// environments
	BEACH,
	FOREST,
	GRASS,
	GROUND,
	MOUNTAIN,
	WATER,
	REEF,
	ROAD,
	// buildings
	CAPITAL,
	CITY,
	FACTORY,
	// unknown in case of different between data and managed code
	UNKNOWN
}
