package Constants;

public enum LoggingLevel {
	ERROR,
	WARNING,
	DEBUG,
	INFO,
	TEST
}
