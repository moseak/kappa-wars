//package Client;
//
//import java.awt.Color;
//import java.awt.GraphicsConfiguration;
//import java.awt.GraphicsDevice;
//import java.awt.GraphicsEnvironment;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Image;
//import java.awt.Insets;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.awt.image.BufferedImage;
//import java.util.ArrayList;
//import java.util.Arrays;
//
//
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
//import javax.swing.JButton;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import Client.CompoundIcon.Axis;
//import Map.Case;
//import other.Grid2D;
//import other.Pair;
//
//public class TestPane extends JPanel{
//
//	private static final long serialVersionUID = 1L;
//	public Color defaultBackground;
//	//public TestPane T;
//	public Grid2D<Case> g2d;
//	public String[][] tab = null;
//	public JLabel[][] cases;
//	public JPanel action;
//
//	public TestPane(JPanel actionWindow) {
//		action = actionWindow;
//		
//		setLayout(new GridBagLayout());
//		GridBagConstraints gbc = new GridBagConstraints();
//		gbc.insets = new Insets(0,0,0,0);
//		//gbc.weightx = 1;
//		// gbc.weighty = 1;
//		vasyJenSaisRien alexis = new vasyJenSaisRien();
//		g2d = alexis.createCaseGridFromFile("../maps/parallele.map");
//		cases = new JLabel[g2d.dimY][g2d.dimX];
//		tab = new String[g2d.dimY][g2d.dimX];
//		for (int row = 0; row < g2d.dimY; row ++)
//			for (int col = 0; col < g2d.dimX; col++)
//				tab[row][col] = "";
//
//		tab[11][4]="unit_recon_blue";
//		ImageIcon iconUnit = new ImageIcon(new ImageIcon("../sprites/"+  tab[11][4] + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
//		String imageName;
//		CompoundIcon iconcomp= null;
//		gbc.fill = GridBagConstraints.BOTH;
//		for (int row = 0; row < g2d.dimY ; row++) {
//			gbc.gridy = row;
//			for (int col = 0; col < g2d.dimX; col++) {
//				JLabel position = new JLabel(); 
//				cases[row][col] = position;
//				gbc.gridx = col;
//				imageName = g2d.get(col, row).data.spriteName;
//				ImageIcon icon;
//
//				icon = new ImageIcon(new ImageIcon("../sprites/"+ imageName + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
//				if (!tab[row][col].equals(""))
//				{ 		
//					iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,iconUnit);        				
//					position.setIcon(iconcomp);               	
//				}
//				else
//				{
//					position.setIcon(icon);
//				}
//				position.setVerticalAlignment(JLabel.BOTTOM);
//				position.setName("case " + row + " " + col);
//
//
//				position.addMouseListener(new MouseAdapter() {
//					@Override
//					public void mousePressed(MouseEvent e) {
//						JLabel j = (JLabel)(e.getSource());	
//
//
//						//j.setIcon(getGray());
//						j.setOpaque(false);
//						ArrayList<Pair<Integer,Integer>> Array = new ArrayList<Pair<Integer,Integer>>();
//						Pair p = new Pair(1,3);
//						Pair p2 = new Pair(11,4);
//						Array.add(p);
//						Array.add(p2);
//						colorPath(Array);
//
//						System.out.println("passage par ici");
//						System.out.println("hello : "+j.getName());
//
//						String[] split = j.getName().split(" ");
//						int x = Integer.parseInt(split[1]);
//						int y = Integer.parseInt(split[2]);
//						System.out.println(tab[x][y]);
//						String message;
//						if (!tab[x][y].equals("")){
//							message = tab[x][y].toString();
//						}
//						else 
//						{				
//							message = g2d.get(y, x).data.spriteName;
//						}
//						
//						 changeActionButton(message);
//						//JOptionPane.showMessageDialog(null, message);
//						if (e.getButton() == MouseEvent.BUTTON3)
//						{
//
//							System.out.println(x +  " " + y);
//							tab[x][y]="unit_recon_blue";
//
//							reloadCase(j);
//							System.out.println(Arrays.deepToString(tab));
//						}
//						if (e.getButton() == MouseEvent.BUTTON2)
//						{
//
//							System.out.println(x +  " " + y);
//							tab[x][y]="";
//
//
//							reloadCase(j);
//							System.out.println(Arrays.deepToString(tab));
//						}
//
//
//					}
//
//
//
//
//
//
//				});
//				//  position
//				add(position, gbc);
//
//			}
//		}
//	}
//	
//	public void reloadCase(JLabel j)
//	{
//		j.getName();
//		String[] split = j.getName().split(" ");
//		int x = Integer.parseInt(split[1]);
//		int y = Integer.parseInt(split[2]);
//		ImageIcon icon = new ImageIcon(new ImageIcon("../sprites/"+ g2d.get(y, x).data.spriteName + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
//		ImageIcon iconUnit = new ImageIcon(new ImageIcon("../sprites/"+  tab[x][y] + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
//		CompoundIcon iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,iconUnit); 
//		j.setIcon(iconcomp);
//	}
//	public Icon highlightCase(Icon icon) {
//
//		
//	
//		
//		
//		
//		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
//		GraphicsDevice gd = ge.getDefaultScreenDevice();
//		GraphicsConfiguration gc = gd.getDefaultConfiguration();
//		BufferedImage image = gc.createCompatibleImage(45,45);
//		
//		
//		int rgb = new Color(51,255,255).getRGB();
//		int[] array = new int[45*45];
//		Arrays.fill(array, rgb);
//		image.setRGB(0, 0,45,45, array, 0, 0);
//		ImageIcon iconColor = new ImageIcon(image);
//		AlphaIcon alphaIcon = new AlphaIcon(iconColor,0.4f);
//		CompoundIcon iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,alphaIcon);        				
//		
//		
//		return iconcomp;
//	}
//
//
//	public void changeActionButton(/*ArrayList<String>*/ String s )
//	{
////	for( String s : array)
//	//	{
//		action.add(new JButton("kappa"));
//		action.revalidate();
//		
//		
//	//	}
//	}
//
//	public void getPossibleActions(int x, int y)
//	{
//		//appel 
//		return;
//
//	}
//
//	public void getPossibleMoves(int x, int y)
//	{
//		return;
//	}
//
//	public void colorPath(ArrayList<Pair<Integer,Integer>> Array)
//	{
//		for(Pair<Integer,Integer> p : Array)
//		{
//			cases[p.x][p.y].setIcon(highlightCase(cases[p.x][p.y].getIcon()));
//		}
//	}
//}