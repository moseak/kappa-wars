package Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;

import Client.CompoundIcon.Axis;
import Map.Case;
import other.Coordinates;
import other.Grid2D;
import other.Pair;

public class Test {
	public JFrame frame;
	public JPanel MainWindow,ActionWindow;
	public TestPane T;
	public JLabel tour;

	public boolean attack = false;
	//	public Grid2D<Case> g2d;
	//	public String[][] tab = null;
	//	public JLabel[][] Cases;
	ArrayList<Pair<Integer,Integer>> arrayOldcases = new ArrayList<Pair<Integer,Integer>>() ;

	Coordinates lastClick = new Coordinates(-1, -1);
	Coordinates lastClickFactory = new Coordinates(0, 0);
	public ArrayList<JButton> buttonList = new ArrayList<JButton>();

	private static int PORT = 8901;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;



	public static void main(String[] args) throws Exception {
		String serverAddress = (args.length == 0) ? "localhost" : args[1];
		Test T1 = new Test(serverAddress);
		T1.play();
	}




	public Test(String serverAddress) throws Exception {

		// Setup networking
		socket = new Socket(serverAddress, PORT);
		in = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
					ex.printStackTrace();
				}

				frame = new JFrame("Testing");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				MainWindow = new JPanel();
				//MainWindow.setBackground(Color.CYAN);
				MainWindow.setLayout(new GridBagLayout());

				JPanel menu = new JPanel();
				menu.setLayout(new GridBagLayout());
				//menu.setBackground(Color.GREEN);
				tour = new JLabel("bonjour");
				menu.add(tour);
				menu.setPreferredSize(new Dimension(200,500));

				JPanel income = new JPanel();
				//income.setBackground(Color.red);


				ActionWindow = new JPanel();
				ActionWindow.setLayout(new GridBagLayout());
				//ActionWindow.setBackground(Color.ORANGE);


				GridBagConstraints contrainteIncome = new GridBagConstraints();
				contrainteIncome.fill = GridBagConstraints.BOTH;
				contrainteIncome.weightx = 0.25;
				contrainteIncome.weighty = 0.25;
				contrainteIncome.gridx = 0;
				contrainteIncome.gridy = 0;
				contrainteIncome.anchor = GridBagConstraints.FIRST_LINE_START;

				GridBagConstraints contrainteAction = new GridBagConstraints();
				contrainteAction.weightx = 1;
				contrainteAction.weighty = 1;
				contrainteAction.gridx = GridBagConstraints.RELATIVE;
				contrainteAction.gridy = 1;
				contrainteAction.fill = GridBagConstraints.HORIZONTAL;
				contrainteAction.anchor = GridBagConstraints.FIRST_LINE_START;



				GridBagConstraints contrainteMenu = new GridBagConstraints();
				contrainteMenu.fill = GridBagConstraints.BOTH;
				contrainteMenu.insets = new Insets(0, 0, 0, 25);

				menu.add(income,contrainteIncome);
				menu.add(ActionWindow,contrainteAction);

				menu.add(income);
				menu.add(ActionWindow);

				T = new TestPane(ActionWindow);
				MainWindow.add(T);
				MainWindow.add(menu,contrainteMenu);
				//MainWindow.add(menu);

				frame.add(MainWindow);


				frame.setResizable(false);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);

				frame.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent windowEvent) {
						out.println("QUIT");
						System.exit(0);
					}
				});

			}
		});
	}



	public void play() throws Exception {
		String response;
		try {
			response = in.readLine();
			if (response.startsWith("WELCOME")) {

				System.out.println("Vous etes: " + response.substring(8));

			}

			String[] splited = response.split("\\s+");
			String commande = splited[0];

			String resu;
			int locationX;
			int locationY;
			int locationX2;
			int locationY2;
			Coordinates coordDepart;
			Coordinates coordArrivee;
			while (true) {
				response = in.readLine();

				if (response.startsWith("KERNEVEZ")) {

				}

				if (response.startsWith("CASE_RESPONSE")) {

					System.out.println(response);
					splited = response.split("\\s+");

					if (splited.length > 1) {
						ArrayList<Pair<Integer, Integer>> listPoss = new ArrayList<Pair<Integer, Integer>>();
						for (int i = 1; i < splited.length; i+=2) {
							listPoss.add(new Pair<Integer, Integer>(Integer.parseInt(splited[i]), Integer.parseInt(splited[i+1])));

						}

						T.colorPath(listPoss);
					}
					else {
						lastClick.x = -1;
						lastClick.y = -1;
					}
				}

				if (response.startsWith("ACTION_RESULT")) {
					System.out.println(response);

					ArrayList<String> actionPoss = new ArrayList<String>();
					splited = response.split("\\s+");
					for (int i = 1; i < splited.length; i++) {
						actionPoss.add(splited[i]);
					}

					T.changeActionButton(actionPoss);
				} 

				if (response.startsWith("UNIT_RESULT")) {
					System.out.println(response);

					ArrayList<String> unitPoss = new ArrayList<String>();
					splited = response.split("\\s+");
					for (int i = 1; i < splited.length; i+=2) {
						unitPoss.add(splited[i] + " " + splited[i+1]);
					}

					T.changeActionButton(unitPoss);
				}

				if (response.startsWith("MOVE_ACCEPTED")) {
					System.out.println(response);
					splited = response.split("\\s+");

					locationX = Integer.parseInt(splited[1]);
					locationY = Integer.parseInt(splited[2]);	

					locationX2 = Integer.parseInt(splited[3]);
					locationY2 = Integer.parseInt(splited[4]);

					T.moveUnit(locationX, locationY, locationX2, locationY2);

				}

				if (response.startsWith("MOVE_REFUSED")) {					
					//out.println("ACTION_REQUEST " + lastClick.x + " " + lastClick.y);			

					lastClick.x = -1;
					lastClick.y = -1;

					for(Pair<Integer,Integer> p : arrayOldcases)
					{
						T.reloadCase(T.cases[p.x][p.y]);
					}

					T.clearPanel();
					out.println("ACTION_REQUEST " + lastClickFactory.x + " " + lastClickFactory.y);

				}

				if (response.startsWith("ATTACK_RANGE")) {
					System.out.println(response);
					splited = response.split("\\s+");

					if (splited.length > 1) {
						ArrayList<Pair<Integer, Integer>> listPoss = new ArrayList<Pair<Integer, Integer>>();
						for (int i = 1; i < splited.length; i+=2) {
							listPoss.add(new Pair<Integer, Integer>(Integer.parseInt(splited[i]), Integer.parseInt(splited[i+1])));

						}

						T.colorPath(listPoss);
						attack = true;
					}
				}

				if (response.startsWith("RESULT_ATTACK")) {
					System.out.println(response);
					splited = response.split("\\s+");

					String unitUn = splited[1];
					locationX = Integer.parseInt(splited[2]);
					locationY = Integer.parseInt(splited[3]);

					String unitDeux = splited[4];
					locationX2 = Integer.parseInt(splited[5]);
					locationY2 = Integer.parseInt(splited[6]);

					if (unitUn.equals("YES")) {
						T.reloadCase(T.cases[locationX][locationY]);
						T.reloadCase(T.cases[locationX2][locationY2]);
					} else if (unitUn.equals("NO")) {
						T.tab[locationX][locationY] = "";

						T.reloadCase(T.cases[locationX][locationY]);
						T.reloadCase(T.cases[locationX2][locationY2]);

					}
					
					if (unitDeux.equals("YES")) {

					} else if (unitDeux.equals("NO")) {
						T.tab[locationX2][locationY2] = "";
						T.reloadCase(T.cases[locationX][locationY]);
						T.reloadCase(T.cases[locationX2][locationY2]);
						
					}
					
					attack = false;
					lastClick.x = -1;
					lastClick.y = -1;
				}

				if (response.startsWith("BUILD_RESULT")) {
					System.out.println(response);
					splited = response.split("\\s+");

					String nomUnit = splited[1];

					T.tab[lastClickFactory.x][lastClickFactory.y] = nomUnit;
					T.reloadCase(T.cases[lastClickFactory.x][lastClickFactory.y]);
				}

				if (response.startsWith("ENDTURN_TRUE")) {
					tour.setText("TOUR ENNEMI");
				}

				if (response.startsWith("OPPONENT_MOVE")) {
					String resuOpponent = response.substring("OPPONENT_MOVE ".length(), response.length()); 
					System.out.println(resuOpponent);
					System.out.println(response);

					if (resuOpponent.startsWith("MOVE_ACCEPTED")) {
						System.out.println(resuOpponent);
						splited = resuOpponent.split("\\s+");

						locationX = Integer.parseInt(splited[1]);
						locationY = Integer.parseInt(splited[2]);	

						locationX2 = Integer.parseInt(splited[3]);
						locationY2 = Integer.parseInt(splited[4]);

						T.moveUnit(locationX, locationY, locationX2, locationY2);

					}

					if (resuOpponent.startsWith("BUILD_RESULT")) {
						System.out.println(resuOpponent);
						splited = resuOpponent.split("\\s+");

						String nomUnit = splited[1];
						locationX = Integer.parseInt(splited[2]);	
						locationY = Integer.parseInt(splited[3]);

						T.tab[locationX][locationY] = nomUnit;
						T.reloadCase(T.cases[locationX][locationY]);
					}

					if (resuOpponent.startsWith("RESULT_ATTACK")) {
						System.out.println(resuOpponent);
						splited = resuOpponent.split("\\s+");

						String unitUn = splited[1];
						locationX = Integer.parseInt(splited[2]);
						locationY = Integer.parseInt(splited[3]);

						String unitDeux = splited[4];
						locationX2 = Integer.parseInt(splited[5]);
						locationY2 = Integer.parseInt(splited[6]);

						if (unitUn.equals("YES")) {
							T.reloadCase(T.cases[locationX][locationY]);
							T.reloadCase(T.cases[locationX2][locationY2]);
						} else if (unitUn.equals("NO")) {
							T.tab[locationX][locationY] = "";

							T.reloadCase(T.cases[locationX][locationY]);
							T.reloadCase(T.cases[locationX2][locationY2]);
						}
						if (unitDeux.equals("YES")) {

						} else if (unitDeux.equals("NO")) {
							T.tab[locationX2][locationY2] = "";

							T.reloadCase(T.cases[locationX][locationY]);
							T.reloadCase(T.cases[locationX2][locationY2]);
						}
					}

					if (resuOpponent.startsWith("ENDTURN_TRUE")) {
						tour.setText("VOTRE TOUR!");
					}

				}

				if (response.startsWith("VALID_MOVE")) {
					break;



				} 


				if (response.startsWith("MESSAGE")) {
					System.out.println(response.substring(8));
				}

			}
			out.println("QUIT");
		}
		finally {
			socket.close();
		}
	}

	public JPanel getActionWindow()
	{
		return ActionWindow;
	}



	public class TestPane extends JPanel{

		private static final long serialVersionUID = 1L;
		public Color defaultBackground;
		//public TestPane T;
		public Grid2D<Case> g2d;
		public String[][] tab = null;
		public JLabel[][] cases;
		public JPanel action;

		public TestPane(JPanel actionWindow) {
			action = actionWindow;

			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0,0,0,0);
			//gbc.weightx = 1;
			// gbc.weighty = 1;
			vasyJenSaisRien alexis = new vasyJenSaisRien();
			g2d = alexis.createCaseGridFromFile("../maps/ile_haricot.map");
			cases = new JLabel[g2d.dimY][g2d.dimX];
			tab = new String[g2d.dimY][g2d.dimX];
			for (int row = 0; row < g2d.dimY; row ++)
				for (int col = 0; col < g2d.dimX; col++)
					tab[row][col] = "";
			
			String imageName;
			CompoundIcon iconcomp= null;
			gbc.fill = GridBagConstraints.BOTH;
			for (int row = 0; row < g2d.dimY ; row++) {
				gbc.gridy = row;
				for (int col = 0; col < g2d.dimX; col++) {
					JLabel position = new JLabel(); 
					cases[row][col] = position;
					gbc.gridx = col;
					imageName = g2d.get(col, row).data.spriteName;
					ImageIcon icon;

					icon = new ImageIcon(new ImageIcon("../sprites/"+ imageName + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
//					if (!tab[row][col].equals(""))
//					{ 		
//						iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,iconUnit);        				
//						position.setIcon(iconcomp);               	
//					}
//					
					position.setIcon(icon);
//					}
					position.setVerticalAlignment(JLabel.BOTTOM);
					position.setName("case " + row + " " + col);


					position.addMouseListener(new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
							JLabel j = (JLabel)(e.getSource());	

							clearPanel();
							j.setOpaque(false);












							String[] split = j.getName().split(" ");
							int x = Integer.parseInt(split[1]);
							int y = Integer.parseInt(split[2]);



							if (attack) {
								out.println("ATTACK_REQUEST " + lastClickFactory.x + " " + lastClickFactory.y + " " + x + " " + y);
							}
							else {

								lastClickFactory.x = x;
								lastClickFactory.y = y;

								if (lastClick.x != -1 && lastClick.y != -1) {
									System.out.println("ON VA FAIRE UN MOVE REQUEST");
									out.println("MOVE_REQUEST " + lastClick.x + " " + lastClick.y + " " + x + " " + y);
								}


								else { 
									System.out.println("passage par ici");
									System.out.println("hello : "+j.getName());

									split = j.getName().split(" ");
									x = Integer.parseInt(split[1]);
									y = Integer.parseInt(split[2]);

									if(!tab[x][y].equals("")) {
										lastClick.x = x;
										lastClick.y = y;
									}
									out.println("CASE_REQUEST " + x + " " + y);

									out.println("ACTION_REQUEST " + x + " " + y);


									System.out.println(tab[x][y]);
									String message;
									if (!tab[x][y].equals("")){
										message = tab[x][y].toString();
									}
									else 
									{				
										message = g2d.get(y, x).data.spriteName;
									}


									//JOptionPane.showMessageDialog(null, message);
									if (e.getButton() == MouseEvent.BUTTON3)
									{
										
										reloadCase(j);
										
									}

								}


							}
						}


					});
					//  position
					add(position, gbc);

				}
			}
		}

		public void reloadCase(JLabel j)
		{
			j.getName();
			String[] split = j.getName().split(" ");
			int x = Integer.parseInt(split[1]);
			int y = Integer.parseInt(split[2]);
			ImageIcon icon = new ImageIcon(new ImageIcon("../sprites/"+ g2d.get(y, x).data.spriteName + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
			if(tab[x][y].equals(""))
			{
				j.setIcon(icon);
			}
			else
			{
			ImageIcon iconUnit = new ImageIcon(new ImageIcon("../sprites/"+  tab[x][y] + ".png").getImage().getScaledInstance(45,45,Image.SCALE_DEFAULT));
			CompoundIcon iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,iconUnit); 
			j.setIcon(iconcomp);
			}
			j.revalidate();
			j.repaint();
		}
		public Icon highlightCase(Icon icon) {






			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gd.getDefaultConfiguration();
			BufferedImage image = gc.createCompatibleImage(45,45);


			int rgb = new Color(51,255,255).getRGB();
			int[] array = new int[45*45];
			Arrays.fill(array, rgb);
			image.setRGB(0, 0,45,45, array, 0, 0);
			ImageIcon iconColor = new ImageIcon(image);
			AlphaIcon alphaIcon = new AlphaIcon(iconColor,0.4f);
			CompoundIcon iconcomp = new CompoundIcon(Axis.Z_AXIS,icon,alphaIcon);        				


			return iconcomp;
		}


		public void clearPanel()
		{
			for( JButton button : buttonList)
			{
				action.remove(button);
				action.revalidate();
				action.repaint();
			}					
			buttonList.clear();
			System.out.println("on vient de remove" + buttonList.toString());
		}

		public void changeActionButton(ArrayList<String> array )
		{



			for( String s : array)
			{
				JButton button = new JButton(s);
				button.setBorder(BorderFactory.createCompoundBorder( BorderFactory.createBevelBorder( BevelBorder.RAISED, Color.BLUE.darker(), Color.BLACK), BorderFactory.createEtchedBorder(EtchedBorder.LOWERED))); button.setFont(new Font("Arial", Font.BOLD, 14));
				button.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						System.out.println(((JButton)e.getSource()).getText());
						clearPanel();
						switch (((JButton)e.getSource()).getText())				
						{
						case "endOfTurn" :
							out.println("ENDTURN_REQUEST");

							break;
						case "createUnit" :
							out.println("UNIT_LIST");							
							break;
						case "wait" :


							break;
						case "attack" :
							out.println("ATTACK_POSSIBLE " + lastClickFactory.x + " " + lastClickFactory.y);

							break;
						case "capture" :

							break;
						default:
							String resu = "BUILD_REQUEST ";

							String[] splited = ((JButton)e.getSource()).getText().split("\\s+");

							resu = resu + splited[0];

							resu = resu + " " + lastClickFactory.x + " " + lastClickFactory.y;

							out.println(resu);
							break;

						}



					}

				}
						);
				buttonList.add(button);	

			}	

			GridBagConstraints contrainteButton = new GridBagConstraints();
			int i=0;

			contrainteButton.weightx = 1;
			contrainteButton.weighty = 1;
			contrainteButton.anchor = GridBagConstraints.FIRST_LINE_START;
			contrainteButton.anchor = GridBagConstraints.CENTER;
			contrainteButton.gridx = 0;
			contrainteButton.gridy = GridBagConstraints.RELATIVE;
			for( JButton button : buttonList)
			{		
				action.add(button,contrainteButton);
			}	

			action.revalidate();

		}



		public void getPossibleActions(int x, int y)
		{
			//appel 
			return;

		}

		public void getPossibleMoves(int x, int y)
		{
			return;
		}

		public void colorPath(ArrayList<Pair<Integer,Integer>> Array)
		{

			for(Pair<Integer,Integer> p : arrayOldcases)
			{
				reloadCase(cases[p.x][p.y]);
			}

			arrayOldcases = Array;
			for(Pair<Integer,Integer> p : Array)
			{
				cases[p.x][p.y].setIcon(highlightCase(cases[p.x][p.y].getIcon()));
			}
		}


		public void moveUnit(int x,int y,int x2,int y2){


			String unit = tab[x][y];
			tab[x][y] = "";
			tab[x2][y2] = unit;

			reloadCase(cases[x2][y2]);
			reloadCase(cases[x][y]);

			lastClick.x = -1;
			lastClick.y = -1;

			for(Pair<Integer,Integer> p : arrayOldcases)
			{
				reloadCase(cases[p.x][p.y]);
			}

		}

	}





}