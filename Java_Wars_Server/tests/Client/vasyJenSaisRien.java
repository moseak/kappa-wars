package Client;

import Data.BasicTxtParser;
import Map.Case;
import Map.CaseData;
import Map.CaseFactory;
import Map.CaseFactory_MapFile;
import Map.Map;
import other.Grid2D;

public class vasyJenSaisRien {
	/**
	 * Parser for basic txt file
	 */
	BasicTxtParser parser;
	/**
	 * Constructor for vasyJenSaisRien
	 */
	public vasyJenSaisRien()
	{
		// instanciation of parser
		parser = new BasicTxtParser();
	}
	/***
	 * Generates data of cases for an ihm map
	 * @param filePath path of the file to generate
	 * @return data in a Grid2D
	 */
	public Grid2D<Case> createCaseGridFromFile(String filePath)
	{
		Grid2D<Case> g2d = null;
		// open file
		if ( parser.openFile(filePath) )
		{
			// 1st line is the name of the map
			String map_name = parser.nextLine();
			// 2nd line for number of players
			int nbPlayers = Integer.parseInt(parser.nextLine());
			// 3r line is for dimensions (x y)
			String temp = parser.nextLine();
			String[] tempSplit = temp.split(" ");
			int x, y;
			x = Integer.parseInt(tempSplit[0]);
			y = Integer.parseInt(tempSplit[1]);
			// rest of file is the 2DGrid
			g2d = new Grid2D<Case>(x, y);
			CaseFactory cf = new CaseFactory_MapFile();
			// read each line of the map
			for(int i=0; i<y; i++)
			{
				temp = parser.nextLine();
				tempSplit = temp.split("\\s+");
				// for each case
				for(int j=0; j<x; j++ )
				{
					g2d.add( i, j, cf.createCaseFromString(tempSplit[j]) );
				}
			}
		}
		return g2d;
	}
}
