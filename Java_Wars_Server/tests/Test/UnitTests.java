package Test;

import Constants.LoggingLevel;
import Logs.LoggerSingleton;

public class UnitTests {
	public static void test_logger()
	{
		LoggerSingleton.log(LoggingLevel.TEST, "UnitTest", "Ceci est un test");
	}
	public static void main(String [] arg)
	{
		test_logger();
	}
}
